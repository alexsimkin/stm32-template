TOOLCHAIN_PREFIX = arm-none-eabi-
CC      = $(TOOLCHAIN_PREFIX)gcc
LD      = $(TOOLCHAIN_PREFIX)ld
OBJCOPY = $(TOOLCHAIN_PREFIX)objcopy
STRIP   = $(TOOLCHAIN_PREFIX)strip

DEF_CFLAGS ?= -g -O0
DEF_CFLAGS += -std=c99
DEF_CFLAGS += -mthumb -mlittle-endian -ffunction-sections -ffreestanding
DEF_CFLAGS += -mcpu=cortex-m3
DEF_CFLAGS += -nostdlib
#DEF_CFLAGS += -nostdinc
DEF_CFLAGS += -DSTM32F10X_MD_VL -DUSE_STDPERIPH_DRIVER -D__ASSEMBLY__ -DSTM32F100RB


# Libraries
DEF_CFLAGS += -Ilibs/CMSIS
DEF_CFLAGS += -Ilibs/CMSIS_boot
DEF_CFLAGS += -Ilibs/SPL


# Project
PROJNAME := $(notdir $(PWD))
SOURCES  := $(wildcard *.c)
OBJECTS   = $(SOURCES:.c=.o)


LDFLAGS  = -mcpu=cortex-m3 -mthumb -mlittle-endian
LDFLAGS += -nostartfiles
#LDFLAGS += --section-start=.text=0x8000000
#LDFLAGS += -Xlinker -Map -Xlinker $(PROJNAME).map -Xlinker
# Run from FLASH
LDFLAGS += -Tlink.ld


# Cortex-M3 Library
CMSIS_SOURCES += $(wildcard libs/CMSIS_boot/*.c)
CMSIS_SOURCES += $(wildcard libs/CMSIS_boot/startup/*.c)
CMSIS_CFLAGS  := $(DEF_CFLAGS)
CMSIS_OBJECTS  = $(CMSIS_SOURCES:.c=.o)


# STM32 Standard Peripheral Library
SPL_SOURCES := $(wildcard libs/SPL/*.c)
SPL_OBJECTS  = $(SPL_SOURCES:.c=.o)
SPL_CFLAGS  := $(DEF_CFLAGS)


.PHONY: clean all load

all: $(OBJECTS) $(CMSIS_OBJECTS) $(SPL_OBJECTS)
	@echo -e "\033[1;32m LD\033[0m" $(PROJNAME)
	@$(CC) $(LDFLAGS) $^ -o $(PROJNAME).elf
#	@$(STRIP) -g -S -d --strip-debug --strip-unneeded $(PROJNAME).elf
	@$(OBJCOPY) -O binary $(PROJNAME).elf $(PROJNAME).bin
#	@$(OBJCOPY) -O ihex $(PROJNAME).elf $(PROJNAME).hex
#	@arm-none-eabi-size $(PROJNAME).elf

%.o: %.c
	@echo -e "\033[1m   CC\033[0m" $<
	@$(CC) $(DEF_CFLAGS) -c $< -o $@

clean:
	@echo "RM All objects"
	@rm -r $(OBJECTS) $(CMSIS_OBJECTS) $(SPL_OBJECTS)
	@rm $(PROJNAME).elf $(PROJNAME).bin $(PROJNAME).hex

# Upload binary to device
write:
	@st-flash write v1 $(PROJNAME).hex 0x8000000
